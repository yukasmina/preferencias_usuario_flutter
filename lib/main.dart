import 'package:flutter/material.dart';
import 'package:preference_usuario/src/page/home_page.dart';
import 'package:preference_usuario/src/page/settings_page.dart';
import 'package:preference_usuario/src/share_preference/preferencia_usuario.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final preference = new PreferenciaUsuario();
  await preference.initPreferences();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final preference = new PreferenciaUsuario();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Preferencias ',
      initialRoute: preference.ultimaPagina,
      routes: {
        HomePage.routeName: (BuildContext context) => HomePage(),
        SettingsPage.routeName: (BuildContext context) => SettingsPage(),
      },
    );
  }
}
