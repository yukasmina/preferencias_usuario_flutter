import 'package:flutter/material.dart';
import 'package:preference_usuario/src/share_preference/preferencia_usuario.dart';
import 'package:preference_usuario/src/widgets/menu_widgets.dart';

class HomePage extends StatelessWidget {
  static final String routeName = 'home';
  final preference = new PreferenciaUsuario();

  @override
  Widget build(BuildContext context) {
    preference.ultimaPagina = HomePage.routeName;
    return Scaffold(
      appBar: AppBar(
        title: Text('Preferencias de Usuario'),
        backgroundColor: (preference.color) ? Colors.teal : Colors.blue,
      ),
      drawer: MenuWidgets(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Color Secundario: ${preference.color}'),
          Divider(),
          Text('Genero ${preference.genero}'),
          Divider(),
          Text('Nombre usuario: ${preference.nombre}'),
          Divider(),
        ],
      ),
    );
  }
}
