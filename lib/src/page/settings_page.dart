import 'package:flutter/material.dart';
import 'package:preference_usuario/src/share_preference/preferencia_usuario.dart';
import 'package:preference_usuario/src/widgets/menu_widgets.dart';

class SettingsPage extends StatefulWidget {
  static final String routeName = 'settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _colorSecundario;
  int _genero;
  String _nombre;
  TextEditingController _textController;
  final preferences = new PreferenciaUsuario();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _genero = preferences.genero;
    _colorSecundario = preferences.color;
    _nombre = preferences.nombre;
    preferences.ultimaPagina = SettingsPage.routeName;
    _textController = new TextEditingController(text: preferences.nombre);
  }

  _setSelectedRadio(int value) {
    preferences.genero = value;
    _genero = value;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
        backgroundColor: (preferences.color) ? Colors.teal : Colors.blue,
      ),
      drawer: MenuWidgets(),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text(
              'Settings',
              style: TextStyle(fontSize: 45.0, fontWeight: FontWeight.bold),
            ),
          ),
          Divider(),
          SwitchListTile(
            //control del switch
            value: _colorSecundario,
            onChanged: (value) {
              setState(() {
                _colorSecundario = value;
                preferences.color = value;
              });
            },
            title: Text('Color secundario'),
          ),
          RadioListTile(
            value: 1,
            title: Text('Masculino'),
            groupValue: _genero,
            onChanged: _setSelectedRadio,
          ),
          RadioListTile(
            value: 2,
            title: Text('Femenino'),
            groupValue: _genero,
            onChanged: _setSelectedRadio,
          ),
          Divider(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(labelText: 'Nombre', helperText: 'Nombre de la persona usando el telefono'),
              onChanged: (value) {
                preferences.nombre = value;
              },
            ),
          )
        ],
      ),
    );
  }
}
