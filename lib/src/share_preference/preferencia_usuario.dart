import 'package:shared_preferences/shared_preferences.dart';

class PreferenciaUsuario {
  //patron singleton
  static final PreferenciaUsuario _instancia = new PreferenciaUsuario._internal();

  factory PreferenciaUsuario() {
    return _instancia;
  }

  PreferenciaUsuario._internal(); //cierra singleton
  SharedPreferences _preferences;

  //ninguna de estas propiedades se usa
/*  bool _colorSeccundario;
  int _genero;
  String _nombre;*/

  initPreferences() async {
    this._preferences = await SharedPreferences.getInstance();
  }

  //get y set genero
  get genero {
    return _preferences.getInt('genero') ?? 1;
  }

  set genero(int value) {
    _preferences.setInt('genero', value);
  }

  //get y set color
  get color {
    return _preferences.getBool('color') ?? false;
  }

  set color(bool value) {
    _preferences.setBool('color', value);
  }

//get y set nombre
  get nombre {
    return _preferences.getString('nombre') ?? '';
  }

  set nombre(String value) {
    _preferences.setString('nombre', value);
  }

  //get y set de la ultimaPagina
  get ultimaPagina {
    return _preferences.getString('ultimaPagina') ?? 'home';
  }

  set ultimaPagina(String value) {
    _preferences.setString('ultimaPagina', value);
  }
}
